# POC Travail de diplôme

Ce projet est un POC (Proof Of Concept) pour le travail de diplôme 2020-2021.

## Mettre en place l'environement de développement

Ce programme utilise `OpenGL`, il est donc recommandé de mettre à jour les drivers de la carte graphique.

Vous pouvez ensuite installer `Visual Studio 2019` et ouvrir le projet dedans.
