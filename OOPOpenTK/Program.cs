﻿// <copyright file="Program.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using System.Diagnostics;
    using System.Reflection;
    using OpenTK.Mathematics;
    using OpenTK.Windowing.Desktop;

    /// <summary>
    /// Entry point of the program.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            FieldInfo[] fields = typeof(Color4).GetFields(BindingFlags.Public | BindingFlags.Instance);
            Debug.WriteLine($"Size : {fields.Length}");
            for (int i = 0; i < fields.Length; i++)
            {
                var field = fields[i];
                Debug.WriteLine($"{i} : {field}");
            }

            GameWindowSettings gameWindowSettings = new GameWindowSettings();

            NativeWindowSettings nativeWindowSettings = new NativeWindowSettings
            {
                Size = new Vector2i(1280, 720),
                Title = "OpenTK OOP",
            };

            using GameWindow game = new GameWindow(gameWindowSettings, nativeWindowSettings);
            game.Run();
        }
    }
}
