﻿// <copyright file="Shader.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using OpenTK.Graphics.OpenGL;

    /// <summary>
    /// Wrapper of OpenGL's shader.
    /// </summary>
    public sealed class Shader
    {
        private readonly int handle;

        /// <summary>
        /// Initializes a new instance of the <see cref="Shader"/> class.
        /// </summary>
        /// <param name="type">Type of the shader.</param>
        /// <param name="code">Source code of the shader.</param>
        public Shader(ShaderType type, string code)
        {
            // Create shader object
            this.handle = GL.CreateShader(type);

            // Set source and compile shader
            GL.ShaderSource(this.handle, code);
            GL.CompileShader(this.handle);
        }

        /// <summary>
        /// Gets the handle of the shader object.
        /// </summary>
        public int Handle { get => this.handle; }
    }
}
