﻿// <copyright file="VertexAttribute.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using OpenTK.Graphics.OpenGL;

    /// <summary>
    /// Container for an OpenGL vertex attribute.
    /// </summary>
    public sealed class VertexAttribute
    {
        private readonly string name;
        private readonly int size;
        private readonly VertexAttribPointerType type;
        private readonly bool normalized;
        private readonly int stride;
        private readonly int offset;

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexAttribute"/> class.
        /// </summary>
        /// <param name="name">Name of the vertex attribute.</param>
        /// <param name="size">Size of the vertex.</param>
        /// <param name="type">Type of the vertex.</param>
        /// <param name="stride">Stride of the vertex.</param>
        /// <param name="offset">Offset of the vertex.</param>
        /// <param name="normalize">True if the vertex should be normalised.</param>
        public VertexAttribute(string name, int size, VertexAttribPointerType type, int stride, int offset, bool normalize = false)
        {
            this.name = name;
            this.size = size;
            this.type = type;
            this.stride = stride;
            this.offset = offset;
            this.normalized = normalize;
        }

        /// <summary>
        /// Sets this attribute to a shader program.
        /// </summary>
        /// <param name="program">ShaderProgram to set this attribute to.</param>
        public void Set(ShaderProgram program)
        {
            // Get location of the attribute from the shader program
            int index = program.GetAttributeLocation(this.name);

            // Enable and set attribute
            GL.EnableVertexAttribArray(index);
            GL.VertexAttribPointer(index, this.size, this.type, this.normalized, this.stride, this.offset);
        }
    }
}