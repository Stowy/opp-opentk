﻿// <copyright file="Vertex.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;
    using OpenTK.Mathematics;

    /// <summary>
    /// Represents a vertex.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Vertex
    {
        /// <summary>
        /// Position of the vertex.
        /// </summary>
        public readonly Vector3 Position;

        /// <summary>
        /// Initializes a new instance of the <see cref="Vertex"/> struct.
        /// </summary>
        /// <param name="position">Position of the vertex.</param>
        public Vertex(Vector3 position)
        {
            this.Position = position;
        }

        /// <summary>
        /// Gets the number of public non static fields of a type.
        /// </summary>
        /// <param name="type">Type get the number of fields of.</param>
        /// <returns>Returns the number of public non static fields of a type.</returns>
        public static int GetNumberOfFields(Type type)
        {
            return type.GetFields(BindingFlags.Public | BindingFlags.Instance).Length;
        }

        //public static VertexAttribute[] GetVertexAttributes()
    }
}
