﻿// <copyright file="ColouredVertex.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using System;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using OpenTK.Graphics.OpenGL;
    using OpenTK.Mathematics;

    /// <summary>
    /// Represents a simple vertex with a position and a colour.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ColouredVertex 
    {
        /// <summary>
        /// Size of the struct in bytes.
        /// </summary>
        public const int Size = (3 + 4) * 4;

        /// <summary>
        /// Position of the vertex.
        /// </summary>
        public readonly Vector3 Position;

        /// <summary>
        /// Color of the vertex.
        /// </summary>
        public readonly Color4 Color;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColouredVertex"/> struct.
        /// </summary>
        /// <param name="position">Position of the vertex.</param>
        /// <param name="color">Color of the vertex.</param>
        public ColouredVertex(Vector3 position, Color4 color)
        {
            this.Position = position;
            this.Color = color;
        }

        public static bool operator ==(ColouredVertex left, ColouredVertex right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ColouredVertex left, ColouredVertex right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Gets the attributes of the this vertex.
        /// </summary>
        /// <returns>Returns the attributes of the this vertex.</returns>
        public static VertexAttribute[] GetVertexAttributes()
        {
            int positionSize = typeof(Vector3).GetFields(BindingFlags.Public | BindingFlags.Instance).Length;
            int colorSize = typeof(Color4).GetFields(BindingFlags.Public | BindingFlags.Instance).Length;

            return new VertexAttribute[]
            {
                new VertexAttribute("vPosition", positionSize, VertexAttribPointerType.Float, Size, 0),
                new VertexAttribute("vColor", colorSize, VertexAttribPointerType.Float, Size, 12),
            };
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            bool equals = false;
            if (obj is ColouredVertex vertex)
            {
                equals = vertex.Position == this.Position || vertex.Color == this.Color;
            }

            return equals;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.Position, this.Color);
        }
    }
}
