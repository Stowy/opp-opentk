﻿// <copyright file="VertexArray.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using OpenTK.Graphics.OpenGL;

    /// <summary>
    /// Vertex Array Object of OpenGL.
    /// VAOs are very different from vertex buffers.
    /// Despite their name, they do not store vertices.
    /// Instead they store information about how to access an array of vertices, which is exactly what we want.
    /// </summary>
    /// <typeparam name="TVertex">Type of the vertices.</typeparam>
    public sealed class VertexArray<TVertex>
        where TVertex : struct
    {
        private readonly int handle;

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexArray{TVertex}"/> class.
        /// </summary>
        /// <param name="vertexBuffer">Vertex buffer for this VAO.</param>
        /// <param name="program">Shader program that is going to use this VAO.</param>
        /// <param name="attributes">Attributes for this VAO.</param>
        public VertexArray(VertexBuffer<TVertex> vertexBuffer, ShaderProgram program, params VertexAttribute[] attributes)
        {
            // Create the new vertex array object
            GL.GenVertexArrays(1, out this.handle);

            // Bind the object so we can modify it
            this.Bind();

            // Bind the vertex buffer object
            vertexBuffer.Bind();

            // Set all attributes
            foreach (var attribute in attributes)
            {
                attribute.Set(program);
            }

            // Unbind objects to reset state
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        /// <summary>
        /// Binds for usage (modification or rendering).
        /// </summary>
        public void Bind()
        {
            GL.BindVertexArray(this.handle);
        }
    }
}
