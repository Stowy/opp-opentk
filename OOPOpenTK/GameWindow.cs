﻿// <copyright file="GameWindow.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using System.Diagnostics;
    using System.IO;
    using OpenTK.Graphics.OpenGL;
    using OpenTK.Mathematics;
    using OpenTK.Windowing.Common;
    using OpenTK.Windowing.Desktop;

    /// <summary>
    /// An OpenGL window.
    /// </summary>
    public sealed class GameWindow : OpenTK.Windowing.Desktop.GameWindow
    {
        private VertexBuffer<ColouredVertex> vertexBuffer;
        private ShaderProgram shaderProgram;
        private VertexArray<ColouredVertex> vertexArray;
        private Matrix4Uniform projectionMatrix;
        private bool isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        /// <param name="gameWindowSettings">Settings for the game window.</param>
        /// <param name="nativeWindowSettings">Settings for the native window.</param>
        public GameWindow(GameWindowSettings gameWindowSettings, NativeWindowSettings nativeWindowSettings)
            : base(gameWindowSettings, nativeWindowSettings)
        {
            Debug.WriteLine($"gl version : " + GL.GetString(StringName.Version));
        }

        /// <inheritdoc/>
        protected override void OnResize(ResizeEventArgs e)
        {
            GL.Viewport(0, 0, e.Width, e.Height);
        }

        /// <inheritdoc/>
        protected override void OnLoad()
        {
            // Create and fill vertex buffer
            this.vertexBuffer = new VertexBuffer<ColouredVertex>(ColouredVertex.Size);

            this.vertexBuffer.AddVertex(new ColouredVertex(new Vector3(-1, -1, -1.5f), Color4.Lime));
            this.vertexBuffer.AddVertex(new ColouredVertex(new Vector3(1, 1, -1.5f), Color4.Red));
            this.vertexBuffer.AddVertex(new ColouredVertex(new Vector3(1, -1, -1.5f), Color4.Blue));

            // Read shaders source
            string vertexSource = File.ReadAllText(@"./shaders/shader.vert");
            string fragmentSource = File.ReadAllText(@"./shaders/shader.frag");

            // Load shaders
            Shader vertexShader = new Shader(ShaderType.VertexShader, vertexSource);
            Shader fragmentShader = new Shader(ShaderType.FragmentShader, fragmentSource);

            // Link shaders in a shader program
            this.shaderProgram = new ShaderProgram(vertexShader, fragmentShader);

            // Create vertex array to specify vertex layout
            this.vertexArray = new VertexArray<ColouredVertex>(
                this.vertexBuffer,
                this.shaderProgram,
                ColouredVertex.GetVertexAttributes());

            // Create projection matrix uniform
            float aspect = this.ClientRectangle.Size.X / this.ClientRectangle.Size.Y;
            this.projectionMatrix = new Matrix4Uniform("projectionMatrix")
            {
                Matrix = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver2, aspect, 0.1f, 100f),
            };
        }

        /// <inheritdoc/>
        protected override void OnUpdateFrame(FrameEventArgs args)
        {
            // This is called every frame, put game logic here
            base.OnUpdateFrame(args);
        }

        /// <inheritdoc/>
        protected override void OnRenderFrame(FrameEventArgs args)
        {
            // Clear the screen
            GL.ClearColor(Color4.Black);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Activate shader program and set uniforms
            this.shaderProgram.Use();
            this.projectionMatrix.Set(this.shaderProgram);

            // Bind vertex bufffer and array objects
            this.vertexBuffer.Bind();
            this.vertexArray.Bind();

            // Upload vertices to GPU and draw them
            this.vertexBuffer.BufferData();
            this.vertexBuffer.Draw();

            // Reset state for potential further draw calls (optional, but good pratice, so mandatory)
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.UseProgram(0);

            // Swap backbuffer
            this.SwapBuffers();
        }

        /// <inheritdoc/>
        protected override void Dispose(bool disposing)
        {
            if (this.isDisposed)
            {
                return;
            }

            if (disposing)
            {
                this.vertexBuffer.Dispose();
            }

            this.isDisposed = true;
        }
    }
}
