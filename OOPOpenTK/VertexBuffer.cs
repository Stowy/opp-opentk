﻿// <copyright file="VertexBuffer.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using System;
    using System.Net;
    using OpenTK.Graphics.OpenGL;

    /// <summary>
    /// Represents an OpenGL Vertex Buffer Object (VBO).
    /// You can think of this as a piece of GPU memory that we can copy our vertices to, so that they can be rendered.
    /// </summary>
    /// <typeparam name="TVertex">The type of the vertices.</typeparam>
    public sealed class VertexBuffer<TVertex> : IDisposable
        where TVertex : struct
    {
        private readonly int vertexSize;
        private readonly int handle;
        private TVertex[] vertices;
        private int count;
        private bool isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexBuffer{TVertex}"/> class.
        /// </summary>
        /// <param name="vertexSize">Size of the vertex.</param>
        public VertexBuffer(int vertexSize)
        {
            this.vertexSize = vertexSize;
            this.vertices = new TVertex[4];

            // Generate the actual Vertex Buffer Object
            this.handle = GL.GenBuffer();
        }

        /// <summary>
        /// Adds a vertex to the buffer.
        /// </summary>
        /// <param name="v">Vertex to add.</param>
        public void AddVertex(TVertex v)
        {
            // Resize the array if it's too small
            if (this.count == this.vertices.Length)
            {
                Array.Resize(ref this.vertices, this.count * 2);
            }

            // Add vertex
            this.vertices[this.count] = v;
            this.count++;
        }

        /// <summary>
        /// Makes this the active array buffer.
        /// </summary>
        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, this.handle);
        }

        /// <summary>
        /// Copies the contained vertices to the GPU memory.
        /// </summary>
        public void BufferData()
        {
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(this.vertexSize * this.count), this.vertices, BufferUsageHint.StreamDraw);
        }

        /// <summary>
        /// Draws the buffered vertices as triangles.
        /// </summary>
        public void Draw()
        {
            GL.DrawArrays(PrimitiveType.Triangles, 0, this.count);
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Deletes the buffer of this VBO.
        /// </summary>
        /// <param name="disposing">True if managed ressources should be disposed.</param>
        private void Dispose(bool disposing)
        {
            if (this.isDisposed)
            {
                return;
            }

            if (disposing)
            {
                GL.DeleteBuffer(this.handle);
            }

            this.isDisposed = true;
        }
    }
}
