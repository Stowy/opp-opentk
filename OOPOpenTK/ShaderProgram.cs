﻿// <copyright file="ShaderProgram.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using OpenTK.Graphics.OpenGL;

    /// <summary>
    /// Shader program that's made of multiple shaders.
    /// </summary>
    public sealed class ShaderProgram
    {
        private readonly int handle;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderProgram"/> class.
        /// </summary>
        /// <param name="shaders">Shaders to be compiled.</param>
        public ShaderProgram(params Shader[] shaders)
        {
            // Create program object
            this.handle = GL.CreateProgram();

            // Assign all the shaders
            foreach (var shader in shaders)
            {
                GL.AttachShader(this.handle, shader.Handle);
            }

            // Link the program (effectively compiles it)
            GL.LinkProgram(this.handle);

            // Detach shaders
            foreach (var shader in shaders)
            {
                GL.DetachShader(this.handle, shader.Handle);
            }
        }

        /// <summary>
        /// Activates this program to be used.
        /// </summary>
        public void Use()
        {
            GL.UseProgram(this.handle);
        }

        /// <summary>
        /// Gets the location of the provided attribute.
        /// </summary>
        /// <param name="name">Name of the attribute.</param>
        /// <returns>Returns the location of the attribute.</returns>
        public int GetAttributeLocation(string name)
        {
            return GL.GetAttribLocation(this.handle, name);
        }

        /// <summary>
        /// Gets the location of the provided uniform.
        /// </summary>
        /// <param name="name">Name of the uniform to find.</param>
        /// <returns>Returns the location of the provided uniform.</returns>
        public int GetUniformLocation(string name)
        {
            return GL.GetUniformLocation(this.handle, name);
        }
    }
}
