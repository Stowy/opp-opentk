﻿// <copyright file="Matrix4Uniform.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace OOPOpenTK
{
    using OpenTK.Graphics.OpenGL;
    using OpenTK.Mathematics;

    /// <summary>
    /// Represents a GLSL Matrix4 uniform.
    /// Parameters like matrices, textures, and other user defined values that remain constant for the execution
    /// of a shader are stored in uniforms, which can in fact be treated like constants within shader code.
    /// </summary>
    public sealed class Matrix4Uniform
    {
        private readonly string name;
        private Matrix4 matrix;

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4Uniform"/> class.
        /// </summary>
        /// <param name="name">Name of the uniform.</param>
        public Matrix4Uniform(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Gets or sets the matrix of this object.
        /// </summary>
        public Matrix4 Matrix { get => this.matrix; set => this.matrix = value; }

        /// <summary>
        /// Sets the uniform of the provided shader program.
        /// </summary>
        /// <param name="program">Shader program that's going to have his uniform modified.</param>
        public void Set(ShaderProgram program)
        {
            // Get uniform location
            int i = program.GetUniformLocation(this.name);

            // Set uniform value
            GL.UniformMatrix4(i, false, ref this.matrix);
        }
    }
}
